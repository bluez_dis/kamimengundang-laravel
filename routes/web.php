<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');
Route::post('/wish', 'HomeController@wish');

Route::group(["prefix" => "dashboard"], function(){
    Route::get('/', 'DashboardController@index');
    Route::resource('bridegrooms', 'BridegroomsController');        
    Route::get('/create_gallery', 'BridegroomsController@create_gallery');    
    Route::post('/gallery', 'BridegroomsController@gallery');    
    Route::get('/bridegroom_images_delete/{id}', 'BridegroomsController@bridegroom_images_delete');
    
    Route::get('/create_wish', 'BridegroomsController@create_wish');    
    Route::post('/wish', 'BridegroomsController@wish');
    Route::get('/bridegroom_wish_delete/{id}', 'BridegroomsController@bridegroom_wish_delete');

    // post
    Route::resource('posts', 'PostController');
    Route::get('/posts/delete/{id}', 'PostController@destroy');

    // users controller
    Route::resource('users', 'UsersController');
    Route::get('/users/delete/{id}', 'UsersController@destroy');

    // user_types controller
    Route::resource('/user_types', 'UserTypesController');
    Route::get('/user_types/delete/{id}', 'UserTypesController@destroy');
});

//static page
Route::get('/cerita/{slug}', 'HomeController@invitation');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
