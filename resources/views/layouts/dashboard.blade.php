<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard Kami Mengundang</title>

    <link href="/backend/common/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="/backend/common/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="/backend/common/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="/backend/common/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="/backend/common/img/favicon.png" rel="icon" type="image/png">
    

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="/backend/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/cleanhtmlvideoplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/datatables/media/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/chartist/dist/chartist.min.css">
    <!-- v1.4.0 -->
    <link rel="stylesheet" type="text/css" href="/backend/vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/backend/vendors/jquery-steps/demo/css/jquery.steps.css">
    <!-- v1.4.2 -->
    <link rel="stylesheet" type="text/css" href="/backend/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <!-- v1.7.0 -->
    <link rel="stylesheet" type="text/css" href="/backend/vendors/dropify/dist/css/dropify.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="/backend/common/css/source/main.css">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="/backend/vendors/jquery/jquery.min.js"></script>
    <script src="/backend/vendors/tether/dist/js/tether.min.js"></script>
    <script src="/backend/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/backend/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="/backend/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="/backend/vendors/spin.js/spin.js"></script>
    <script src="/backend/vendors/ladda/dist/ladda.min.js"></script>
    <script src="/backend/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="/backend/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="/backend/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="/backend/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="/backend/vendors/autosize/dist/autosize.min.js"></script>
    <script src="/backend/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="/backend/vendors/moment/min/moment.min.js"></script>
    <script src="/backend/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/backend/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/backend/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="/backend/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="/backend/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="/backend/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="/backend/vendors/summernote/dist/summernote.min.js"></script>
    <script src="/backend/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/backend/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="/backend/vendors/nestable/jquery.nestable.js"></script>
    <script src="/backend/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/backend/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="/backend/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="/backend/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="/backend/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="/backend/vendors/d3/d3.min.js"></script>
    <script src="/backend/vendors/c3/c3.min.js"></script>
    <script src="/backend/vendors/chartist/dist/chartist.min.js"></script>
    <script src="/backend/vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
    <script src="/backend/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- v1.1.1 -->
    <script src="/backend/vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="/backend/vendors/hackertyper/hackertyper.js"></script>
    <script src="/backend/vendors/jquery-countTo/jquery.countTo.js"></script>
    <!-- v1.4.0 -->
    <script src="/backend/vendors/nprogress/nprogress.js"></script>
    <script src="/backend/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <!-- v1.4.2 -->
    <script src="/backend/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/backend/vendors/chart.js/src/Chart.bundle.min.js"></script>
    <!-- v1.7.0 -->
    <script src="/backend/vendors/dropify/dist/js/dropify.min.js"></script>

    <!-- Clean UI Scripts -->
    <script src="/backend/common/js/common.js"></script>
    <script src="/backend/common/js/demo.temp.js"></script>

    
</head>
<body class="theme-default">
<nav class="left-menu" left-menu>
    <div class="logo-container">
        <a href="/" class="logo">
            <img src="/backend/common/img/logo.png" alt="Clean UI Admin Template" />
            <img class="logo-inverse" src="/backend/common/img/logo-inverse.png" alt="Clean UI Admin Template" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        @include('partial.sidebar')
    </div>
</nav>

<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu">    
        <div class="menu-user-block">
            <div class="dropdown dropdown-avatar">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="avatar" href="javascript:void(0);">
                        <img src="/backend/common/img/temp/avatars/1.jpg" alt="Alternative text to the image">
                    </span>
                </a>
                
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">                    
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();
                                     return confirm('anda yakin akan keluar?');">
                        <i class="dropdown-icon icmn-exit"></i>
                        {{ __('Sign Out') }}
                    </a>                                                 
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>

        <div class="menu-info-block">
            <div class="right hidden-md-down margin-left-20">
                <div class="search-block">
                    <div class="form-input-icon form-input-icon-right">
                        <i class="icmn-search"></i>
                        <input type="text" class="form-control form-control-sm form-control-rounded" placeholder="Search...">
                        <button type="submit" class="search-block-submit "></button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</nav>

<section class="page-content">
<div class="page-content-inner">
    @yield('content')
</div>


<!-- Page Scripts -->
<script>
    $(function() {

        ///////////////////////////////////////////////////////////
        // COUNTERS
        $('.counter-init').countTo({
            speed: 1500
        });

        ///////////////////////////////////////////////////////////
        // ADJUSTABLE TEXTAREA
        autosize($('#textarea'));


        ///////////////////////////////////////////////////////////
        // DATATABLES
        $('#example1').DataTable({
            responsive: true,
            "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
            "ordering": false
        });


    });
</script>
<!-- End Page Scripts -->
</section>

<div class="cwt__footer visible-footer">    
    <div class="cwt__footer__bottom">
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-8">
                <div class="cwt__footer__company">
                    <img class="cwt__footer__company-logo" src="/backend/common/img/temp/mediatec.png" target="_blank" title="Mediatec Software">
                    <span>
                        © 2020 <a href="#" class="cwt__footer__link" target="_blank">Sigesit Kominfo</a>
                        <br>
                        All rights reserved
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="main-backdrop"><!-- --></div>

</body>
</html>