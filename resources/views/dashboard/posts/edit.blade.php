@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Post</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="panel-body">
        <div class="row">        
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <br />
                    <!-- Horizontal Form -->                                           
                    <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Judul Post</label>
                        </div>
                        <div class="col-md-10">
                            <input id="title" type="text" class="form-control" name="title"  placeholder="Input Judul" value="{{$post->title}}" autofocus>
                        </div>
                    </div>                    
                    @if($post->image)
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Gambar Post</label>
                        </div>
                        <div class="col-md-4">
                            <img id="image" src="/{{$post->image}}" alt="{{$post->image}}" style="width:100%;max-width:300px">                                
                        </div>
                        <div class="col-md-6">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-10">
                            <p class="text-red">* Upload jika ingin merubah gambar post</p>
                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>                        
                    @else
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Gambar Post</label>
                        </div>
                        <div class="col-md-8">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>                            
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-xs-2">
                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                            <p class="text-red">* Format File : jpeg, jpg, png</p>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Body</label>
                        </div>
                        <div class="col-md-10">
                            <textarea class="form-control summernote" id="body" name="body" placeholder="Input Isi Post" rows="3" id="l15">
                                {{$post->body}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Tanggal Post</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                    <label class="input-group datepicker-only-init">
                                        <input type="text" id="post_date"  class="form-control datepicker-only-init" name="post_date" value="{{$post->post_date}}" placeholder="Tanggal Post" autofocus/>
                                        <span class="input-group-addon">
                                            <i class="icmn-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                        </div>
                    </div>  
                    
                    <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Status Post</label>
                            </div>
                            <div class="col-md-10">
                                <select id="status_id" type="text" class="selectpicker form-control{{ $errors->has('status_id') ? ' is-invalid' : '' }}" name="status_id"  placeholder="Input status_id" data-live-search="true" required autofocus>
                                    <option>Pilih Status Post</option>                                
                                    <option value="1" @if($post->status_id==1) selected @endif >Active</option>
                                    <option value="0" @if($post->status_id==0) selected @endif >Draft</option>                                    
                                </select>

                                @if ($errors->has('status_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('status_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                                                                                                     
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Create') }}
                </button>
                <a class="btn btn-success" href="/dashboard/post/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();

        $('.summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection