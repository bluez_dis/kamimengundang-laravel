@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Pengantin Pria dan Wanita</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/bridegrooms') }}" enctype="multipart/form-data">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="margin-bottom-50">                                        
                    <h4>Data Pengantin Wanita</h4>
                    <br />
                    <!-- Horizontal Form -->                                        
                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                         @csrf                        
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_name" type="text" class="form-control" name="bride_name" value="{{ $bridegroom->bride_name }}" placeholder="Input Nama" autofocus readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Qoute</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_qoute" type="text" class="form-control" name="bride_qoute" value="{{ $bridegroom->bride_name }}"  placeholder="Input Nama" autofocus readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Instagram</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_instagram" type="text" class="form-control" name="bride_instagram" value="{{ $bridegroom->bride_instagram }}"  placeholder="Input Nama" autofocus readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Foto</label>
                            </div>
                            <div class="col-md-8">
                                @if($bridegroom->bride_photo)
                                <img id="image" src="/{{$bridegroom->bride_photo}}" alt="{{$bridegroom->name}}" style="width:100%;max-width:300px">
                                @else
                                <p class="text-red">* gambar belum tersedia</p>
                                @endif
                            </div>
                        </div>                                                                                                                     
                </div>
            </div>
            <div class="col-lg-6">
                <div class="margin-bottom-50">                    
                    <h4>Data Pengantin Pria</h4>
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_name" type="text" class="form-control" name="groom_name" value="{{ $bridegroom->groom_name }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_qoute" type="text" class="form-control" name="groom_qoute" value="{{ $bridegroom->groom_qoute }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Instagram</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_instagram" type="text" class="form-control" name="groom_instagram" value="{{ $bridegroom->groom_instagram }}"  placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Foto</label>
                        </div>
                        <div class="col-md-8">
                            @if($bridegroom->groom_photo)
                            <img id="image" src="/{{$bridegroom->groom_photo}}" alt="{{$bridegroom->groom_name}}" style="width:100%;max-width:300px">
                            @else
                            <p class="text-red">* gambar belum tersedia</p>
                            @endif
                        </div>
                    </div>  
                                                                                                                     
                </div>
            </div>
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <!-- <h4>Data Pengantin Pria</h4> -->
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Center</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute" type="text" class="form-control" name="qoute" value="{{ $bridegroom->qoute }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Detail</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute_detail" type="text" class="form-control" name="qoute_detail" value="{{ $bridegroom->qoute_detail }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Tanggal Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <input id="wedding_date" type="text" class="form-control" name="wedding_date" value="{{ $bridegroom->wedding_date }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lokasi Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <input id="location_weeding" type="text" class="form-control" name="location_weeding" value="{{ $bridegroom->location_weeding }}" placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lat</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_lat" type="text" class="form-control" name="location_lat" value="{{ $bridegroom->location_lat }}" placeholder="Input Nama" autofocus readonly>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Long</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_long" type="text" class="form-control" name="location_long" value="{{ $bridegroom->location_long }}" placeholder="Input Nama" autofocus readonly>
                        </div>                       
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Image Slider Utama</label>
                        </div>
                        <div class="col-md-8">
                            @if($bridegroom->image_slider)
                            <img id="image" src="/{{$bridegroom->image_slider}}" alt="{{$bridegroom->groom_name}}" style="width:100%;max-width:300px">
                            @else
                            <p class="text-red">* gambar belum tersedia</p>
                            @endif
                        </div>
                    </div>  
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Slug Request</label>
                        </div>
                        <div class="col-md-8">
                            <input id="slug" type="text" class="form-control" name="slug" value="{{ $bridegroom->slug }}"  placeholder="Input Nama" autofocus readonly>
                        </div>
                    </div>                                                     
                                                                                                                     
                </div>
            </div>
        </div>          

        <!-- Gallery -->
        <section class="panel panel-with-borders">
            <div class="panel-heading">
                <div class="heading-buttons pull-right">
                    @if($bridegroom->bridegroom_images->count() != 8)
                    <a class='btn btn-primary' href='{{url('/dashboard/create_gallery?bridegroom_id='.$bridegroom->id)}}'><i class="icmn-plus3"></i>Add Picture</a>                    
                    @endif
                </div>
                <h3>Gallery</h3>
            </div>
            <div class="panel-body">
                <div class="app-gallery clearfix">
                @foreach($bridegroom->bridegroom_images as $bridegroom_images)                        
                    <div class="app-gallery-item edit" style="background-image: url({{url($bridegroom_images->image)}})">
                        <div class="app-gallery-item-hover">
                            <div class="btn-group margin-inline">
                                <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('dashboard/bridegroom_images_delete')}}/{{$bridegroom_images->id}}'><i class="icmn-bin3"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach                    
                </div>
            </div>
        </section>
        <!-- End Gallery -->
        
        <!-- Wish -->
        <section class="panel panel-with-borders">
            <div class="panel-heading">
                <div class="heading-buttons pull-right">
                    <a class='btn btn-primary' href='{{url('/dashboard/create_wish?bridegroom_id='.$bridegroom->id)}}'><i class="icmn-plus3"></i>Add Wish</a>
                </div>
                <h3>Wish For You</h3>
            </div>
            <div class="panel-body">
                <div class="app-gallery clearfix">                                        
                    <div class="col-xs-12">&nbsp;</div>                    
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Wish For You</th>
                                <th width="15%">Aksi</th>
                              </tr>
                            <tbody>
                                @foreach($bridegroom->bridegroom_wishes as $bridegroom_wish)
                                <tr>
                                    <td>{{$bridegroom_wish->name}}</td>
                                    <td>{{$bridegroom_wish->email}}</td>
                                    <td>{{$bridegroom_wish->wish}}</td>
                                    <td>
                                        <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('dashboard/bridegroom_wish_delete')}}/{{$bridegroom_wish->id}}'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>            
                </div>
            </div>
        </section>
        <!-- End Wish -->

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
            <a class='btn btn-primary' href='{{url('/dashboard/bridegrooms/'.$bridegroom->id.'/edit')}}'>Edit</a>
            <a class="btn btn-success" href="/dashboard/bridegrooms/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(window).load(function() {
        $('#prizePopup').modal('show');
    });
    
    $(function(){
        $('.select2').select2();        
    })
</script>
@endsection