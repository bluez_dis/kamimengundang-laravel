@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data Pengantin Pria dan Wanita</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/bridegrooms') }}" enctype="multipart/form-data">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="margin-bottom-50">                                        
                    <h4>Data Pengantin Wanita</h4>
                    <br />
                    <!-- Horizontal Form -->                                        
                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                         @csrf                        
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_name" type="text" class="form-control" name="bride_name"  placeholder="Input Nama" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Qoute</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_qoute" type="text" class="form-control" name="bride_qoute"  placeholder="Input Nama" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Instagram</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_instagram" type="text" class="form-control" name="bride_instagram"  placeholder="Input Nama" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="form-control-label" for="l0">Foto</label>
                            </div>
                            <div class="col-md-8">
                                <input id="bride_photo" type="file" class="form-control" name="bride_photo"  placeholder="Input Nama" autofocus>
                            </div>
                        </div>                                                                                                                     
                </div>
            </div>
            <div class="col-lg-6">
                <div class="margin-bottom-50">                    
                    <h4>Data Pengantin Pria</h4>
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_name" type="text" class="form-control" name="groom_name"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_qoute" type="text" class="form-control" name="groom_qoute"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Instagram</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_instagram" type="text" class="form-control" name="groom_instagram"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Foto</label>
                        </div>
                        <div class="col-md-8">
                            <input id="groom_photo" type="file" class="form-control" name="groom_photo"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>  
                                                                                                                     
                </div>
            </div>
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <!-- <h4>Data Pengantin Pria</h4> -->
                    <br />
                    <!-- Horizontal Form -->                                           
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Center</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute" type="text" class="form-control" name="qoute"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Qoute Detail</label>
                        </div>
                        <div class="col-md-8">
                            <input id="qoute_detail" type="text" class="form-control" name="qoute_detail"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Tanggal Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <input id="wedding_date" type="text" class="form-control" name="wedding_date"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lokasi Acara Pernikahan</label>
                        </div>
                        <div class="col-md-8">
                            <input id="location_weeding" type="text" class="form-control" name="location_weeding"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Lat</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_lat" type="text" class="form-control" name="location_lat"  placeholder="Input Nama" autofocus>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Long</label>
                        </div>
                        <div class="col-md-4">
                            <input id="location_long" type="text" class="form-control" name="location_long"  placeholder="Input Nama" autofocus>
                        </div>                       
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Image Slider Utama</label>
                        </div>
                        <div class="col-md-8">
                            <input id="image_slider" type="file" class="form-control" name="image_slider"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>  
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-control-label" for="l0">Slug Request</label>
                        </div>
                        <div class="col-md-8">
                            <input id="slug" type="text" class="form-control" name="slug"  placeholder="Input Nama" autofocus>
                        </div>
                    </div>                                                     
                                                                                                                     
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Create') }}
                </button>
                <a class="btn btn-success" href="/dashboard/bridegrooms/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();
    })
</script>
@endsection