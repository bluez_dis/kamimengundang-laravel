@extends('layouts.dashboard')

@section('content')
<section class="page-content">
<div class="page-content-inner">    
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>
                Data User
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="col-md-4">
                        <a href="/dashboard/users/create" class="btn btn-primary">
                            Tambahkan Users
                        </a>
                    </div>
                    
                    <div class="col-xs-12">&nbsp;</div>                    
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                              <tr>                                
                                <th>Nama Users</th>
                                <th>Email</th>
                                <th width="15%">Aksi</th>
                              </tr>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{ $user->name}}</td>                                   
                                    <td>
                                        <a class='btn btn-success btn-xs' href='{{url('dashboard/users/'.$user->id)}}'>show</a> 
                                        <a class='btn btn-primary btn-xs' href='{{url('dashboard/users/'.$user->id.'/edit')}}'>Edit</a>
                                        <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('dashboard/users/delete')}}/{{$user->id}}'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>
    $(function(){
        $('.datepicker').datetimepicker({
            format:  "YYYY-MM-DD"
        });
    });
</script>
<!-- End Page Scripts -->
</section>
@endsection