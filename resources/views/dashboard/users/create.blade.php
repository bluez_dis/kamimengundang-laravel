@extends('layouts.dashboard')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Form Data User</h3>
    </div>
    <br />
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/users') }}" enctype="multipart/form-data">
    <div class="panel-body">
        <div class="row">        
            <div class="col-lg-12">
                <div class="margin-bottom-50">                    
                    <br />
                    <!-- Horizontal Form -->                                           
                    <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Nama</label>
                        </div>
                        <div class="col-md-10">
                            <input id="name" type="text" class="form-control" name="name"  placeholder="Input nama user" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Gambar</label>
                        </div>
                        <div class="col-md-10">
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Email</label>
                        </div>
                        <div class="col-md-10">
                            <input id="email" type="text" class="form-control" name="email"  placeholder="Input email user" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Password</label>
                        </div>
                        <div class="col-md-10">
                            <input id="password" type="password" class="form-control" name="password"  placeholder="Input password user" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="form-control-label" for="l0">Confirm Password</label>
                        </div>
                        <div class="col-md-10">
                            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"  placeholder="Input confirm password user" autofocus>
                        </div>
                    </div>                                                                                   
                </div>
            </div>
        </div>  

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Create') }}
                </button>
                <a class="btn btn-success" href="/dashboard/users/">Back</a>
            </div>
        </div>
    </div>
    </form>
    <!-- End Horizontal Form -->      
</section>
<!-- End -->
<script>
    $(function(){
        $('.select2').select2();

        $('.summernote').summernote({
            height: 350
        });

        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection