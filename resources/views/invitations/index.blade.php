@extends('layouts.application')

@section('image', url($bridegroom->image_slider))
@section('title', $bridegroom->slug)
@section('caption', $bridegroom->groom_name.' & '.$bridegroom->bride_name)
@section('permalink', 'https://kamimengundang.id/cerita/'.$bridegroom->slug)

@section('css')
  <style>
      .iframe-container{
            position: relative;
            width: 100%;
            padding-bottom: 56.25%; /* Ratio 16:9 ( 100%/16*9 = 56.25% ) */
        }
        .iframe-container > *{
            display: block;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }
  </style>
@endsection

@section('js')
    <script>
        $('#clock').countdown('{{date("Y-m-d", strtotime($bridegroom->wedding_date))}}', function (event) {
            $(this).html(event.strftime(
                '<div class="countdown_wrap d-flex"><div  class="single_countdown"><h3>%D</h3><span>Days</span></div><div class="single_countdown"><h3>%H</h3><span>Hours</span></div><div class="single_countdown"><h3>%M</h3><span>Minutes</span></div><div class="single_countdown"><h3>%S</h3><span>Seconds</span></div></div>'
            ));
        });

        $(window).on('load', function() {
            $('#exampleModalProtocol').modal('show');
        });
    </script>
@endsection

@section('content')
<!-- slider_area -->
<div class="slider_area ">
    <div class="slider_area_inner overlay2" style="background-image: url({{ url($bridegroom->image_slider) }});">
        <div class="slider_text text-center">
            <div class="text_inner">
                <img src="/frontend/img/banner/ornaments.png" alt=""><br>                
                <h4>{{date("d-M-Y", strtotime($bridegroom->wedding_date))}}</h4>
                <span>Renjana</span>
                <h3>{{$bridegroom->nickname_bride}} & {{$bridegroom->nickname_groom}} </h3>                
            </div>
        </div>
    </div>
</div>
<!--/ slider_area -->

<!-- wedding_countdown -->
<div class="weeding_countdown_area">
    <div class="flowaers_top d-none d-lg-block">
        <img src="/frontend/img/banner/flower-top.png" alt="">
    </div>
    <div class="flowaers_bottom d-none d-lg-block">
            <img src="/frontend/img/banner/flower-bottom.png" alt="">
        </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">                        
                    <span>{{date("d-M-Y", strtotime($bridegroom->wedding_date))}}</span>
                    <h3>THE WEDDING Countdown</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div id="clock" class="countdown_area counter_bg ">
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ wedding_countdown -->

<!-- our_love-story -->
<div class="love_story_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">
                    <h3>Our Love Story</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="single_lover_story text-center">
                    <div class="story_thumb">
                        <img src="/{{$bridegroom->bride_photo}}" alt="" style="border-radius:50%;">
                    </div>
                    <span>Bride</span>
                    <h3>{{$bridegroom->bride_name}}</h3>
                    <p>{{$bridegroom->bride_qoute}}</p>
                    <div class="social_links">
                        <ul>
                            <!-- <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                            <li><a href="#"> <i class="fa fa-twitter"></i> </a></li> -->
                            <li><a href="{{$bridegroom->bride_instagram}}"> <i class="fa fa-instagram"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-lg-4">
                <div class="weding_time_line text-center">                    
                    </br>
                    </br>
                    </br>
                    </br>
                    <div class="single_time_line">
                        <h4>{{$bridegroom->qoute}}</h4>                        
                    </div>                    
                    </br>
                    </br>
                    </br>
                    </br>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single_lover_story text-center">
                    <div class="story_thumb">
                    <img src="/{{$bridegroom->groom_photo}}" alt="" style="border-radius:50%;">
                    </div>
                    <span>Groom</span>
                    <h3>{{$bridegroom->groom_name}}</h3>
                    <p>{{$bridegroom->groom_qoute}}</p>
                    <div class="social_links">
                        <ul>
                            <!-- <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                            <li><a href="#"> <i class="fa fa-twitter"></i> </a></li> -->
                            <li><a href="{{$bridegroom->groom_instagram}}"> <i class="fa fa-instagram"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ our_love-story -->

<!-- gallery_area  -->
<div class="gallery_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">
                    <h3>Photo Gallery</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="row grid no-gutters">
            @foreach($bridegroom->bridegroom_images as $bridegroom_image)
            <div class="col-xl-4 col-md-6 col-lg-6 grid-item">
                <div class="single_gallery" style="background-image: url({{url($bridegroom_image->image)}});">
                    <a class="popup-image" href="{{url($bridegroom_image->image)}}">
                    </a>
                </div>
            </div>
            @endforeach            
        </div>
        <!-- <div class="grid row">
            <div class="single_image grid-item">
                <img src="/frontend/img/gallery/1.png" alt="">
            </div><div class="single_image grid-item">
                <img src="/frontend/img/gallery/2.png" alt="">
            </div><div class="single_image grid-item">
                <img src="/frontend/img/gallery/3.png" alt="">
            </div>
        </div> -->
    
    
    </div>
</div>
<!--/ gallery_area  -->

<!-- program_details -->
<div class="program_details_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">
                </div>
            </div>
        </div>
        <div class="row">           
            <div class="col-xl-12 col-lg-12">
                <div class="single_program program_bg_2 text-center">
                    <div class="program_inner ">
                        <h3>
                            <!-- Tanpa mengurangi rasa hormat, Kami memohon doa restu untuk pernikahan kami kepada teman-teman sekalian. Kami mohon maaf tidak dapat mengundang teman-teman ke acara pernikahan kami dikarenakan situasi dan kondisi pada saat ini yang kurang memungkinkan.  -->
                            Akad</br>
                            {{ date('j-F-Y', strtotime($bridegroom->wedding_date))}}</br>
                            {{$bridegroom->akad}}</br>
                            <img src="/frontend/img/program_details/ornaments.png" alt=""></br>
                            Wedding Reception</br>
                            {{ date('j-F-Y', strtotime($bridegroom->wedding_date))}}</br>
                            Sesion 1 {{$bridegroom->session_1}}</br>
                            Sesion 2 {{$bridegroom->session_2}}</br>
                            Sesion 3 {{$bridegroom->session_3}}</br>
                        </h3>                        
                        <img src="/frontend/img/program_details/ornaments.png" alt="">
                        </br>
                        <a href="#" class="boxed_btn3 info-border circle arrow" data-toggle="modal" data-target="#exampleModalCenter">Send Gift<span
						class="lnr lnr-arrow-right"></span></a>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</div>
<!--/ program_details -->

<!-- Modal bank account-->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="program_details_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section_title text-center">
                            <img src="/frontend/img/banner/flowers.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">           
                    <div class="col-xl-12 col-lg-12">
                        <div class="single_program program_bg_2 text-center">
                            <div class="program_inner ">
                                <h3>                                    
                                    {{$bridegroom->bank_account}}</br>                                    
                                </h3>                        
                                <img src="/frontend/img/program_details/ornaments.png" alt="">
                                </br>
                                <a href="#" class="boxed_btn3 info-border circle arrow" data-dismiss="modal">Close<span
						            class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
        </div>

    </div>
  </div>
</div>

<!-- Modal attention covid protocol-->
<div class="modal fade bd-example-modal-lg" id="exampleModalProtocol" tabindex="-1" role="dialog" aria-labelledby="exampleModalProtocol" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="program_details_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section_title text-center">
                            <img src="/frontend/img/banner/flowers.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">           
                    <div class="col-xl-12 col-lg-12">
                        <div class="single_program program_bg_2 text-center">
                            <div class="program_inner ">
                                <h3>
                                    Dihimbau untuk para tamu undangan untuk tetap mematuhi protokol kesehatan.</br>
                                        <img class="img-fluid" src="/frontend/img/protocol_1.png" alt=""></br>
                                        <img class="img-fluid" src="/frontend/img/protocol_2.png" alt="">                                
                                </h3>                        
                                <img src="/frontend/img/program_details/ornaments.png" alt="">
                                </br>
                                <a href="#" class="boxed_btn3 info-border circle arrow" data-dismiss="modal">Close<span
						            class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
        </div>

    </div>
  </div>
</div>


<!-- ================ contact section start ================= -->
<div class="program_details_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">
                    <h3>Location</h3>
                </div>
            </div>
            <div class="iframe-container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.3859196989606!2d107.91304401477248!3d-6.844253395053742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68d1fdd57913a1%3A0x983fb69660c55d26!2sGedung%20Nusantara!5e0!3m2!1sid!2sid!4v1640183581781!5m2!1sid!2sid" width="100%" height="650" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>                                                    
        </div>
    </div>
</div>  
<!-- ================ contact section end ================= -->

<?php if ($bridegroom->wedding_date >= date('Y-m-d H:i:s')){?>
<!-- attend_area -->
<div class="attending_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1">
                <div class="main_attending_area">
                    <div class="flower_1 d-none d-lg-block">
                        <img src="/frontend/img/appointment/flower-top.png" alt="">
                    </div>
                    <div class="flower_2 d-none d-lg-block">
                        <img src="/frontend/img/appointment/flower-bottom.png" alt="">
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-7 col-lg-8">
                            <div class="popup_box ">
                                <div class="popup_inner">
                                    <div class="form_heading text-center">
                                        <h3>Are You Attending?</h3>
                                    </div>
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/wish') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" class="form-control" name="bridegroom_id" id="bridegroom_id" value="{{ $bridegroom->id }}">
                                        <div class="row">                                              
                                            <div class="col-xl-12">
                                                <input id="name" type="text" class="form-control" name="name"  placeholder="Your Name" required>
                                            </div>
                                            <div class="col-xl-12">
                                                <select name="attending" class="form-select wide" id="default-select" required>
                                                    <option value="0" data-display="Are You Attending?">Are You Attending?</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                            <div class="col-xl-12">
                                                <select name="session_id" class="form-select wide" id="default-select" required>
                                                    <option value="0" data-display="Session">Session</option>
                                                    <option value="1">Session 1 {{$bridegroom->session_1}}</option>
                                                    <option value="2">Session 2 {{$bridegroom->session_2}}</option>
                                                    <option value="3">Session 3 {{$bridegroom->session_3}}</option>
                                                </select>
                                            </div>
                                            <div class="col-xl-12">
                                                <input id="email" type="text" class="form-control" name="email"  placeholder="Email" required>
                                            </div>                                            
                                                                                       
                                            <div class="col-xl-12">
                                                <textarea id="wish" class="form-control" name="wish" placeholder="Your Wish" required></textarea>
                                            </div>
                                            <div class="col-xl-12">
                                                <button type="submit" class="boxed_btn3">Send</button>
                                            </div>                                                                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / attend_area -->
<?php  } ?>

<div class="program_details_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center">
                    <img src="/frontend/img/banner/flowers.png" alt="">
                    <h3>Wish For Us</h3>
                </div>
            </div>            
            <div class="col-xl-12" style="height:350px;overflow-y: scroll;">
                <div class="comments-area">
                    @foreach($bridegroom->bridegroom_wishes as $bridegroom_wish)
                    <div class="comment-list">
                        <div class="single-comment justify-content-between d-flex">
                        <div class="user justify-content-between d-flex">
                            
                            <div class="desc">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                    <h5>
                                        <a href="#">{{ $bridegroom_wish->name }}</a>
                                    </h5>
                                    <p class="date">{{date("d-M-Y", strtotime( $bridegroom_wish->created_at))}} </p>
                                    </div>                                    
                                </div>
                                <p class="comment">
                                    {!! $bridegroom_wish->wish !!}
                                </p>
                            </div>
                        </div>
                        </div>
                    </div>
                    @endforeach                        
                </div>
            </div>
        </div>
    </div>
</div>   

<!-- <audio controls autoplay>
  <source src="/uploads/MUARA_Adera.ogg" type="audio/ogg">
  <source src="/uploads/MUARA_Adera.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio> -->
<iframe allow="autoplay" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/SmKHnd81oqs?autoplay=1&mute=1&origin=https://kamimengundang.id" title="Bug Week " height="0" data-ytbridge="vidSurrogate2" style="width: 0%;"></iframe>

@endsection