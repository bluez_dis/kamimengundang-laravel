<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bridegroom extends Model
{
    use HasFactory;

    public function bridegroom_images()
    {
        return $this->hasMany('App\Models\BridegroomImage');
    }

    public function bridegroom_wishes()
    {
        return $this->hasMany('App\Models\BridegroomWish');
    }
}
