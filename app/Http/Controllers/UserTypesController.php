<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\UserType;

class UserTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_types = UserType::all();
        return view("dashboard.user_types.index",compact(["user_types"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.user_types.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $user = new UserType;                
        $user->name = $input['name'];                                
        $user->save();
        \Session::flash('success','Data User Type berhasil dibuat');
        return redirect("/dashboard/user_types/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_type = UserType::find($id);
        return view("dashboard.user_types.edit",compact(["user_type"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = UserType::find($id);
        $user->name = $input['name'];                                
        $user->save();
        \Session::flash('success','Data User Type berhasil dibuat');
        return redirect("/dashboard/user_types/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = UserType::find($id);
        $user->delete();
        \Session::flash('success','User Type berhasil di dalete');

        return redirect("/dashboard/user_types/");
    }
}
