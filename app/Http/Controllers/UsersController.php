<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use File;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view("dashboard.users.index",compact(["users"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.users.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $random = Str::random(5);
        
        $user = new User;                
        $user->name = $input['name'];                     
        $user->email = $input['email'];                     
        $user->password = bcrypt($input['password']);
        if(isset($input['image'])){
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/users/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/users/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $user->image = $destinationPath.'/'.$filename;
            }
        }        
        
        $user->save();
        \Session::flash('success','Data User berhasil dibuat');
        return redirect("dashboard/users/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view("dashboard.users.show",compact(["user"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view("dashboard.users.edit",compact(["user"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::find($id);
        $user->name = $input["name"];
        $user->email = $input["email"];
        if (!empty($input["password"])) {
            $user->password = bcrypt($input["password"]);
        }
        if ($request->file('image')) {
            if(!is_null($user->image)){
                unlink($user->image);                
            }
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/users/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/users/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $user->image = $destinationPath.'/'.$filename;
            }            
        }
        $user->save();
        \Session::flash('success','User berhasil di update');
        return redirect("/dashboard/users/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!is_null($user->image)){
            unlink($user->image);                
        }
        $user->delete();
        \Session::flash('success','User berhasil di dalete');

        return redirect("users/");
    }
}
